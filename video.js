let video = document.querySelector("#video_orig");
let canvas = document.querySelector("#canva_foto");
let boton_capturar = document.querySelector("#capturar");
let boton_filtro = document.querySelector("#filtro");
let boton_lightness = document.querySelector("#ligthness");
let boton_average = document.querySelector("#average");
let boton_blur = document.querySelector("#blur");
let boton_clear = document.querySelector("#clear");

console.log(video.width);

// Ancho y altura del canvas
canvas.width = canvas.scrollWidth;
canvas.height = canvas.scrollHeight;

// Event Listeners

boton_capturar.addEventListener('click', capturaImagen);
boton_filtro.addEventListener('click', modificaImagen);
boton_lightness.addEventListener('click', modificaImagen);
boton_average.addEventListener('click', modificaImagen);
boton_blur.addEventListener('click', blur);
boton_clear.addEventListener('click', clearImagen);

function capturaImagen () {
    video.pause();
    let ctx = canvas.getContext('2d');
    //var img = new Image();
    ctx.drawImage(video, 0, 0,video.width,video.height);  
}

function clearImagen () {
    let ctx = canvas.getContext('2d');
    ctx.clearRect(0,0,video.width,video.height);
}

function blur() {

    // NO ES BLUR - > Invierte la imagen
    console.log("BLUR");
    //clearImagen();
    //capturaImagen();
    let ctx = canvas.getContext('2d');
    //let imgData = ctx.getImageData(0, 0, canvas.width,canvas.height);
    ctx.scale(-1, 1);
    //ctx.filter = 'blur(4px)';
    ctx.drawImage(video, 0, 0,video.width*-1,video.height);  
    //ctx.putImageData(imgData, 0, 0);
}

function modificaImagen (event) {
    filterName = event.target.id;
    console.log (filterName);
    clearImagen();
    capturaImagen();
    let ctx = canvas.getContext('2d');
    //ctx.drawImage(video, 0, 0);
    console.log (canvas.width);
    ctx.crossOrigin = "Anonymous";
    let imgData = ctx.getImageData(0, 0, video.width,video.height);
    let data = imgData.data;
    console.log (data);

    
    for (let i = 0; i < data.length; i += 4) {
        /*let grey = (0.2126 * data[i]) + (0.7152 * data[i + 1]) + (0.0722 * data[i + 2]);
        data[i] = grey;
        data[i + 1] = grey;
        data[i + 2] = grey;
        */
       pixelRGBA = {
        r: data[i],
        g: data[i+1],
        b: data[i+2],
        o: data[i+3]
        }

        //let convertedPixel = pixelRGBA.r * 0.2126 + pixelRGBA.g * 0.7152 + pixelRGBA.b * 0.0722;
        let convertedPixel = convertPixel (filterName,pixelRGBA);
        data[i] = data[i+1] = data[i+2] = convertedPixel;
    }
    
    ctx.putImageData(imgData, 0, 0);
    //ctx.drawImage(imgData, 0, 0,video.width,video.height); 
}

function convertPixel (filterName, pixelRGBA)
{
    if (filterName == "filtro") {
        return (pixelRGBA.r * 0.2126 + pixelRGBA.g * 0.7152 + pixelRGBA.b * 0.0722);
    }
    if (filterName == "ligthness") {
        return (Math.max(pixelRGBA.r, pixelRGBA.g, pixelRGBA.b) + Math.min(pixelRGBA.r, pixelRGBA.g, pixelRGBA.b) ) / 2; 
    }
    if (filterName == "average") {
        return (pixelRGBA.r + pixelRGBA.g + pixelRGBA.b )/3;
    }
}